# Copyright 2010 Paul Seidler <bl4sph3my@online.de>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'glibmm-2.22.1.exheres-0', which is:
#     Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="C++ bindings for GNOME's Accessibility Toolkit"
HOMEPAGE="https://www.gtkmm.org"

LICENCES="
    LGPL-2.1 [[ note = [ The library itself ] ]]
    GPL-2 [[ note = [ MSVC gendef tool ] ]]
"
SLOT="1.6"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    disable-deprecated  [[ description = [ Omit deprecated API from the library ] ]]
    doc
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.5]
        virtual/pkg-config[>=0.20]
        doc? (
            app-doc/doxygen
            dev-lang/perl:*[>=5.6.0]
            dev-libs/libxslt
            media-gfx/graphviz
        )
    build+run:
        dev-cpp/libsigc++:2[>=2.0.0]
        dev-libs/at-spi2-core[>=2.52.0]
        gnome-bindings/glibmm:2.4[>=2.46.2]
        !gnome-bindings/gtkmm:2.4[<2.21.0] [[
            description = [ older gtkmm includes C++ bindings for atk ]
            resolution = uninstall-blocked-after
        ]]
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    '!disable-deprecated build-deprecated-api'
    'doc build-documentation'
)

src_prepare() {
    meson_src_prepare

    # Fix docdir
    edo sed \
        -e "/install_docdir/s:/ book_name:/ '${PNVR}':" \
        -i doc/reference/meson.build
}

