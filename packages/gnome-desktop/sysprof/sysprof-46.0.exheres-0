# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings gtk-icon-cache freedesktop-desktop freedesktop-mime
require meson

SUMMARY="Sysprof kernel based performance profiler for Linux"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.76.0]
        dev-libs/libadwaita:1[>=1.4.0]
        dev-libs/libdex:1[>=0.3]
        dev-libs/libpanel:1.0[>=1.3.0]
        dev-libs/libunwind
        sys-apps/systemd[>=222]
        sys-auth/polkit:1[>=0.105]
        x11-libs/cairo
        x11-libs/graphene
        x11-libs/pango
        x11-libs/gtk:4.0[>=4.10]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddevelopment=false
    -Dexamples=false
    -Dgtk=true
    -Dhelp=true
    -Dlibsysprof=true
    -Dsysprofd=bundled
    -Dtests=false
    -Dtools=true
)

RESTRICT=test

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

