# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require gsettings gtk-icon-cache
require freedesktop-desktop freedesktop-mime
require test-dbus-daemon
require meson

SUMMARY="Access and organize files"
DESCRIPTION="
Files, also known as Nautilus, is the default file manager of the GNOME desktop.
It provides a  simple and integrated way of managing your files and browsing your file system.

Nautilus supports all the basic functions of a file manager and more.
It can search and manage your files and folders, both locally and on a network,
read and write data to and from removable media, run scripts, and launch apps.
It has three views: Icon Grid, Icon List, and Tree List.
Its functions can be extended with plugins and scripts.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    extensions [[ description = [ Build image and audio/video property pages and sendto plugin ] ]]
    gtk-doc
    wayland
    X
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        gtk-doc? ( dev-doc/gi-docgen )
    build+run:
        app-pim/tracker:3.0
        app-pim/tracker-miners:3.0
        dev-libs/appstream-glib
        dev-libs/at-spi2-core[>=2.52.0]
        dev-libs/glib:2[>=2.79.0][gobject-introspection]
        dev-libs/libadwaita:1[>=1.6.0]
        dev-libs/libcloudproviders[>=0.3.1]
        dev-util/desktop-file-utils
        gnome-desktop/gnome-autoar[>=0.4.4]
        gnome-desktop/gnome-desktop:4[>=43][legacy]
        gnome-desktop/gobject-introspection:1[>=0.6.4]
        gnome-desktop/gsettings-desktop-schemas[>=42]
        sys-libs/libportal[>=0.7][providers:gtk4]
        sys-libs/libseccomp
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.30.0]
        x11-libs/graphene:1.0
        x11-libs/gtk:4.0[>=4.15.2][X?][wayland?]
        x11-libs/libnotify[>=0.7]
        x11-libs/pango[>=1.28.3]
        extensions? (
            dev-libs/gexiv2[>=0.14.2]
            media-plugins/gst-plugins-base:1.0
        )
    run:
        gnome-desktop/gvfs
    recommendation:
        gnome-desktop/gnome-disk-utility [[ note = [ required for disk management ] ]]
    suggestion:
        gnome-desktop/adwaita-icon-theme[>=1.1.91]
"

# Fails some tests:
#  7/14 test-file-operations-trash-or-delete           FAIL             0.19s   killed by signal 5 SIGTRAP
#  8/14 test-file-operations-copy-files                FAIL             0.20s   killed by signal 5 SIGTRAP
#  9/14 test-file-operations-move-files                FAIL             0.23s   killed by signal 5 SIGTRAP
# because of
#
# # Start of test-copy-one-file tests
# Bail out! GVFS-RemoteVolumeMonitor-FATAL-WARNING: remote volume monitor with dbus name
# org.gtk.vfs.UDisks2VolumeMonitor is not supported
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcloudproviders=true
    -Dintrospection=true
    -Dpackagekit=false
    -Dselinux=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'extensions'
    'gtk-doc docs'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=headless -Dtests=none'
)

src_test() {
    esandbox allow_net --bind "unix:${TEMP%/}/gvfsd/socket-*"

    test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net --bind "unix:${TEMP%/}/gvfsd/socket-*"
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

