# Copyright 2021 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] python [ blacklist=2 multibuild=false ] meson

SUMMARY="A documentation generator for GObject-based libraries"

LICENCES="Apache-2.0 CCPL-Attribution-ShareAlike-3.0 CC0 GPL-3 OFL-1.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

print_toml_deps() {
    local abi
    for abi in ${PYTHON_FILTERED_ABIS}; do
        ever at_least 3.11 "${abi}" || echo "python_abis:${abi}? ( dev-python/tomli[python_abis:${abi}] )"
    done
}

DEPENDENCIES="
    build+run:
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/Markdown[>=3.2][python_abis:*(-)?]
        dev-python/MarkupSafe[python_abis:*(-)?]
        dev-python/packaging[python_abis:*(-)?]
        dev-python/Pygments[python_abis:*(-)?]
        dev-python/typogrify[python_abis:*(-)?]
        $(print_toml_deps)
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddevelopment_tests=false
)

src_prepare() {
    meson_src_prepare

    # Fix shebang
    edo sed -e "1c #!/usr/$(exhost --target)/bin/python$(python_get_abi)" \
            -i gi-docgen.py
}

src_install() {
    meson_src_install
    python_bytecompile
}

