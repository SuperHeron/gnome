# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require bash-completion
require vala [ vala_dep=true with_opt=true ]
require gsettings test-dbus-daemon
require meson [ cross_prefix=true ]

SUMMARY="A tool designed to extract information and metadata about your personal data"
HOMEPAGE="http://www.tracker-project.org/"

LICENCES="GPL-2 LGPL-2.1"
SLOT="3.0"
PLATFORMS="~amd64 ~armv8 ~x86"

MYOPTIONS="
    gtk-doc [[ note = [ requires gobject-introspection ] ]]
    systemd [[ description = [ install systemd user services ] ]]
    vapi [[ note = [ requires gobject-introspection ] ]]

    (
        providers:
            soup2
            soup3
    ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        dev-lang/perl:*[>=5.8.1]
        virtual/pkg-config[>=0.20]
        gnome-desktop/gobject-introspection:1[>=0.9.5]
        gtk-doc? (
            dev-doc/gi-docgen
            media-gfx/graphviz[svg]
        )
    build+test:
        gnome-bindings/pygobject:3
    build+run:
        core/json-glib[>=1.4]
        dev-libs/glib:2[>=2.52.0][gobject-introspection]
        dev-db/sqlite:3[>=3.20.0]
        dev-libs/icu:=[>=4.8.1.1] [[
            note = [ upstream prefers libunistring, but we need ICU for charset detection anyway ]
        ]]
        dev-libs/libxml2:2.0[>2.6]
        media-libs/gstreamer:1.0
        media-libs/libmediaart:2.0[>=1.9.0][gobject-introspection]
        media-libs/libpng:=[>=1.2]
        media-plugins/gst-plugins-base:1.0
        sys-apps/dbus[>1.3.1]
        text-libs/libstemmer
        x11-libs/gdk-pixbuf:2.0[>=2.12.0]
        providers:soup2? ( gnome-desktop/libsoup:2.4[>2.40] )
        providers:soup3? ( gnome-desktop/libsoup:3.0[>=2.99.2] )
        !app-pim/tracker:1.0 [[ description = [ Older ABI version, many file collissions ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-gi-skip-tracker-endpoint-http.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Davahi=disabled
    -Dsoup=soup3
    -Dstemmer=enabled
    -Dunicode_support=icu

    # Can be made optional again once when the following issue is resolved:
    # https://gitlab.gnome.org/GNOME/tracker/-/issues/356
    -Dintrospection=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    vapi
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bash_completion'
    'gtk-doc docs'
    'systemd systemd_user_services'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

# Tests are disabled because they are fragile (whether they pass or not depends
# on the environement - they pass for me, but not on CI) and when they do fail,
# the build will hang.
RESTRICT="test"

src_test() {
    unset DISPLAY

    # redirect any configuration and user settings to temp via XDG variables
    export XDG_DATA_HOME=${TEMP}
    export XDG_CACHE_HOME=${TEMP}
    export XDG_CONFIG_HOME=${TEMP}

    # G_DBUS_COOKIE_SHA1_KEYRING_DIR requires 0700, ${TEMP} is 0755
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR_IGNORE_PERMISSION=1
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR=${TEMP}

    # use the memory backend for settings to ensure that the system settings in dconf remain
    # untouched by the tests
    export GSETTINGS_BACKEND=memory

    # the tracker-dbus/request test relies on g_debug() messages being output to stdout
    export G_MESSAGES_DEBUG=all

    # Unix sockets for temporary dbus created in the ${WORKBASE}/_build directory
    esandbox allow_net 'unix-abstract:./dbus-*'

    TZ=UTC LANG=C LC_ALL=C HOME="${TEMP}" test-dbus-daemon_run-tests meson_src_test

    esandbox disallow_net 'unix-abstract:./dbus-*'
}

