# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true ]
require meson
require test-dbus-daemon

SUMMARY="Secret Service dbus client library"
HOMEPAGE="https://wiki.gnome.org/Projects/Libsecret"

LICENCES="LGPL-2"
SLOT="1"
MYOPTIONS="
    bash-completion
    debug
    gobject-introspection
    gtk-doc [[ requires = gobject-introspection ]]
    tpm2 [[ description = [ Bind secrets to hardware using TPM2 ] ]]
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.29] )
        gtk-doc? ( dev-doc/gi-docgen[>=2021.7] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libgcrypt[>=1.2.2]
        !gnome-desktop/libsecret:0 [[
            description = [ slot move ]
            resolution = uninstall-blocked-after
        ]]
        bash-completion? ( app-shells/bash-completion )
        tpm2? ( app-crypt/tpm2-tss[>=3.0.3] )
    test:
        dev-python/dbus-python
"

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dcrypto=libgcrypt'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'debug debugging'
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    tpm2
    vapi
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'bash-completion bash_completion'
)

src_prepare() {
    meson_src_prepare

    # unclear what's up with test-attributes, it's hitting a critical but not failing as expected.
    edo sed -e "/'test-attributes',/d" -i "${MESON_SOURCE}"/libsecret/meson.build
}

src_test() {
    # secret-tool and libegg tests attempt to access tpm2 hardware.
    test-dbus-daemon_run-tests meson test --print-errorlogs \
        --no-suite=secret-tool \
        --no-suite=libegg
}

